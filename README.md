
用Node.js实现的用户中心，实现了单点登录服务、日志服务、钉钉用户和部门的管理等功能，配合企业信息化开发框架CmPage进行多业务子系统的实现和部署，数据库采用MySql，模块配置数据库和CmPage项目共用。

本系统实现了多个业务子系统中用户注册登录等公共部分的功能，各系统间可以用户共享，具体业务的权限设置包括账套设置各自单独进行，结合CmPage的分库和外部模块调用等功能，可以实现业务模块间的解耦，增加开发配置和系统部署的灵活性。


运行步骤简述如下（具体参照 thinkjs.org）：
1、在Mysql中新建数据库sso并导入备份文件（/db/sso_workbench.sql）
2、在/src/common/config/db.js 中配置数据库连接参数
3、运行：npm install --registry=https://registry.npm.taobao.org --verbose
4、运行：npm start
5、访问：http://localhost:8310

演示地址： http://139.129.48.131:8310/admin
