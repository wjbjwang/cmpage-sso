'use strict';
// +----------------------------------------------------------------------
// | CmPage-sso [ CmPage 用户中心 ]
// +----------------------------------------------------------------------
// | Licensed under the Apache License, Version 2.0
// +----------------------------------------------------------------------
// | Author: defans <defans@sina.cn>
// +----------------------------------------------------------------------

/**
 @module api.controller
 */

/**
 * 钉钉对外API接口,版本：v1.1
 返回码：
 200	正常
 300	业务逻辑异常
 301	服务器端系统异常
 302	账号不存在
 303	登录认证失败
 304	账号密码不正确
 305	登录ip与上次不一致
 306	token 超时
 * @class api.controller.v1
 */
import Base from './base.js';
import moment from 'moment';

export default class extends Base {




}
