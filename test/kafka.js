
var kafka = require('kafka-node');
//var HighLevelConsumer = kafka.HighLevelConsumer;
var Consumer = kafka.Consumer;
var Offset = kafka.Offset;
var Client = kafka.Client;

var client = new Client('192.168.1.125:2181');
var topics = [{
        topic: 'test',
        partition: 0,
        offset: 0
    }],
    options = {
        autoCommit: false,
        groupId:'test-consumer-group',
        //fetchMaxWaitMs: 1000,
        //fetchMaxBytes: 1024 * 1024,
        fromOffset: true
    };

var consumer = new Consumer(client, topics, options);

//consumer.setOffset(topic, 0, 36);
consumer.on('message', function(message) {
    var obj = message;
    console.log('msg offset: '+message.offset);
    console.log('----: '+message.value);

});

consumer.on('error', function(err) {
    console.log('error', err);
});
